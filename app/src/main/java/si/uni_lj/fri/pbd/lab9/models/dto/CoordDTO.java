package si.uni_lj.fri.pbd.lab9.models.dto;

import com.google.gson.annotations.SerializedName;

public class CoordDTO {

    @SerializedName("Lat")
    private float lat;

    @SerializedName("Lon")
    private float lon;

    // Getters
    public float getLat() {
        return lat;
    }

    public float getLon() {
        return lon;
    }

    // Setters
    public void setLat(float lat) {
        this.lat = lat;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }
}
