package si.uni_lj.fri.pbd.lab9;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.uni_lj.fri.pbd.lab9.models.dto.CoordDTO;
import si.uni_lj.fri.pbd.lab9.models.dto.WeatherResponseDTO;
import si.uni_lj.fri.pbd.lab9.models.dto.WeatherResponsesDTO;
import si.uni_lj.fri.pbd.lab9.rest.RestAPI;
import si.uni_lj.fri.pbd.lab9.rest.ServiceGenerator;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener{

    private static final String TAG = MapsActivity.class.getSimpleName();
    private GoogleMap mMap;
    private RestAPI mRestClient;

    // Connectivity manager
    private ConnectivityManager mNwManager;
    // Callback called on state changes
    private ConnectivityManager.NetworkCallback mNwCallback;
    // Indicator of network state metered/unmetered
    private boolean mOnUnmetered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mRestClient = ServiceGenerator.createService(RestAPI.class);
        mOnUnmetered = false;
        mNwCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onCapabilitiesChanged(@NonNull Network network, @NonNull NetworkCapabilities networkCapabilities) {
                super.onCapabilitiesChanged(network, networkCapabilities);

                if(networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_NOT_METERED))
                    mOnUnmetered = true;
                else
                    mOnUnmetered = false;

            }
        };

        mNwManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        mNwManager.registerDefaultNetworkCallback(mNwCallback);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mNwManager.unregisterNetworkCallback(mNwCallback);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // subscribe to "move" event
        mMap.setOnCameraIdleListener(this);

        // Add a marker in Sydney and move the camera
        /*
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        */

        mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(46, 14.5),8.0f) );
    }

    /**
     * Called every time the user moves the map
     */
    @Override
    public void onCameraIdle() {

        LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;

        // “longitude-left,latitude-bottom,longitude-right,latitude-top,zoom”
        String bbox = bounds.northeast.longitude+","+bounds.southwest.latitude+","+
                        bounds.southwest.longitude+","+bounds.northeast.latitude+","+10;

        if(mOnUnmetered) {
            // run data download on a background thread with Retrofit method enqueue
            mRestClient.getCurrentWeatherData(bbox, Constants.API_KEY)
                    .enqueue(new Callback<WeatherResponsesDTO>() {
                        @Override
                        public void onResponse(Call<WeatherResponsesDTO> call, Response<WeatherResponsesDTO> response) {

                            if (response.isSuccessful()) {

                                ArrayList<WeatherResponseDTO> locations = response.body().getWeatherResponses();
                                for (int i = 0; i < locations.size(); i++) {
                                    String name;
                                    Float lat, lon, temp;

                                    name = locations.get(i).getName();
                                    lat = locations.get(i).getCoordinates().getLat();
                                    lon = locations.get(i).getCoordinates().getLon();
                                    temp = locations.get(i).getMainInfo().getTemp();

                                    mMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(lat, lon))
                                            .title(name + " " + temp)
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED
                                            )));
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<WeatherResponsesDTO> call, Throwable t) {

                        }
                    });
        } else {
            Toast.makeText(this, "Connect to an unmetered network, please.", Toast.LENGTH_LONG);
        }
    }
}
