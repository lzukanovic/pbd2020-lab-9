package si.uni_lj.fri.pbd.lab9.models.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WeatherResponseDTO {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("coord")
    @Expose
    CoordDTO coordinates;

    @SerializedName("main")
    @Expose
    MainDTO mainInfo;

    // Getters
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public CoordDTO getCoordinates() {
        return coordinates;
    }

    public MainDTO getMainInfo() {
        return mainInfo;
    }

    // Setters
    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
