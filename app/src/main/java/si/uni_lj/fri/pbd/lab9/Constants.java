package si.uni_lj.fri.pbd.lab9;

public class Constants {
    public static final String API_KEY = "OPEN_WEATHER_APP_API_KEY";
    public static final String BASE_URL = "https://api.openweathermap.org/";

}
